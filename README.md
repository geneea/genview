# Genview

Simple web GUI for trying out Geneea Generator. 

## Configuration

The application runs against the Generator REST API, for which it has to be authorized. User name and password can be set using `GEN_USER` and `GEN_PASSWD ` environment variables or via analogical entries in `.env` file:

```
GEN_USER=user
GEN_PASSWD=******
```

## Run

First install the application using `pip`, preferably in a virtual environment.

Simple local run: `python -m genview.app`

Multi-worker run using `gunicorn`:

`gunicorn --bind 0.0.0.0:<port> -w 2 -k uvicorn.workers.UvicornWorker genview.app:app`
