import re

import aiohttp
import typing
import ujson
import uvicorn

from pathlib import Path
from typing import List, Optional, Tuple

from starlette.applications import Starlette
from starlette.middleware import Middleware
from starlette.middleware.gzip import GZipMiddleware
from starlette.routing import Route
from starlette.templating import Jinja2Templates

from genview import settings

templates = Jinja2Templates(directory=str((Path(__file__).parent / 'templates').resolve()))

GENERATOR_URL = f'{settings.GEN_URL}/generate'


async def create_api_client_session():
    global gen_req_sess
    gen_req_sess = aiohttp.ClientSession(
        auth=aiohttp.BasicAuth(login=str(settings.GEN_USER), password=str(settings.GEN_PASSWD), encoding='utf-8'),
        headers={'Content-type': 'application/json; charset=utf-8'},
        json_serialize=ujson.dumps
    )


def get_templates(text: str) -> List[typing.Mapping[str, str]]:
    parts = re.split(r'----\s+([-_a-zA-Z0-9]+)\s+-{8,}', text)
    if len(parts) == 1:
        parts = ['genview', parts[0]]  # no separator found
    elif len(parts) % 2 == 0:
        parts = ['genview', text]  # this should never happen
    else:
        parts = parts[1:]  # drop the part in front of the first marker
    return [
        {
            'id': parts[2 * idx],
            'body': parts[2 * idx + 1].strip()
        }
        for idx in range(int(len(parts) / 2))
    ]


async def call_generator(
        url: str, tmpl_text: str, data: typing.Mapping, domain: Optional[str]
) -> Tuple[str, List[str]]:
    request = {
        'templates': get_templates(tmpl_text),
        'data': data
    }
    if domain and domain != 'default':
        request['domain'] = domain

    text, messages = '', []
    async with gen_req_sess.post(f'{url.rstrip("/")}/generate', json=request) as resp:

        if 400 <= resp.status < 600:
            try:
                resp_dict = await resp.json(loads=ujson.loads, encoding='utf-8')
                messages = [resp_dict['message']]
            except:
                messages = [f'Error {resp.status} when calling generator service']
        else:
            try:
                resp_dict = await resp.json(loads=ujson.loads, encoding='utf-8')
                text = resp_dict['article']
                messages = resp_dict.get('messages', [])
            except:
                messages = ['Could not read generator response']
        return text, messages


async def get_status(url: str) -> str:
    async with gen_req_sess.get(f'{url.rstrip("/")}/status') as resp:
        if 400 <= resp.status < 600:
            return f'Error {resp.status} when calling generator service'
        else:
            resp_dict = await resp.json(loads=ujson.loads, encoding='utf-8')
            return resp_dict.get('message') or resp.text


async def homepage(request):
    template = 'index.html'

    form = await request.form()
    if form:
        generator_url = form.get('generator_url') or str(settings.GEN_URL)
        user_tmpl_text = form.get('tmpl') or ''
        is_data_ok = False
        try:
            user_tmpl_data_str = form.get('indata') or '{}'
            user_tmpl_data = ujson.loads(user_tmpl_data_str)
            is_data_ok = True
        except:
            error_msg = 'Invalid JSON data'
        domain = form.get('domain') or settings.DOMAIN_DEFAULT

        if is_data_ok:
            generated_text, messages = await call_generator(
                url=generator_url, tmpl_text=user_tmpl_text, data=user_tmpl_data, domain=domain)
        else:
            generated_text, messages = '', [error_msg]

        api_status = await get_status(generator_url)

        tmpl_args = {
            'tmpl': user_tmpl_text,
            'indata': ujson.dumps(user_tmpl_data, ensure_ascii=False, indent=2) if is_data_ok else user_tmpl_data_str,
            'generator_url': generator_url,
            'domain': domain,
            'domain_list': settings.GV_DOMAIN_LIST,
            'genresult': generated_text,
            'messages': messages,
            'api_status': api_status,
        }
    else:
        tmpl_args = {
            'tmpl': '',
            'indata': '{}',
            'genresult': '',
            'generator_url': settings.GEN_URL,
            'domain': settings.DOMAIN_DEFAULT,
            'domain_list': settings.GV_DOMAIN_LIST,
            'messages': [],
            'api_status': ''
        }
    tmpl_args['request'] = request
    return templates.TemplateResponse(template, tmpl_args)


async def not_found(request, exc):
    """
    Return an HTTP 404 page.
    """
    template = '404.html'
    context = {'request': request}
    return templates.TemplateResponse(template, context, status_code=404)


async def server_error(request, exc):
    """
    Return an HTTP 500 page.
    """
    template = '500.html'
    context = {'request': request}
    return templates.TemplateResponse(template, context, status_code=500)


app = Starlette(
    routes=[Route('/', endpoint=homepage, methods=['GET', 'POST'])],
    exception_handlers={
        404: not_found,
        500: server_error
    },
    debug=settings.DEBUG,
    middleware=[
        Middleware(GZipMiddleware)
    ],
    on_startup=[create_api_client_session]
)

if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=settings.GV_PORT)
