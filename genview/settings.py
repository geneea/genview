from starlette.config import Config
from starlette.datastructures import URL, Secret

DOMAIN_DEFAULT = 'default'

config = Config(".env")

DEBUG = config('DEBUG', cast=bool, default=False)

GV_PORT = config('GV_PORT', cast=int, default=8000)

# list of domains to use for auto-suggestions of domain name
GV_DOMAIN_LIST = config('GV_DOMAIN_LIST', cast=str.split, default=DOMAIN_DEFAULT)
if DOMAIN_DEFAULT not in GV_DOMAIN_LIST:
    GV_DOMAIN_LIST = [DOMAIN_DEFAULT] + GV_DOMAIN_LIST

GEN_USER = config('GEN_USER', cast=Secret)
GEN_PASSWD = config('GEN_PASSWD', cast=Secret)

GEN_URL = config('GEN_URL', cast=URL, default='https://generator.geneea.com')
