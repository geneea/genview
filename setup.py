# coding=utf-8

"""
Set up packaging, unit tests and dependencies.
"""

from setuptools import find_packages, setup

setup(
    name='genview',
    version='0.7.0',

    author='Geneea Analytics',
    description='Simple test environment for Geneea generator',
    keywords='natural language generation',
    url='https://geneea.com',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.8',
        'License :: OSI Approved :: Apache Software License'
    ],

    # Dependencies
    install_requires=[
        'Jinja2~=3.1', 'starlette>=0.20', 'aiohttp~=3.8', 'uvicorn>=0.18', 'python-multipart>=0.0.5', 'ujson>=5.4'
    ],


    packages=find_packages(exclude=['*.test', '*.test.*', 'test.*', 'test']),

    package_data={
        'genview.templates': ['*.html']
    },

    test_suite='genview'
)
